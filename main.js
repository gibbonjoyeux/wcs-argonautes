
// https://codepen.io/wildcodeschool/pen/LYpoBBN

function	crew_update(dom_crew, crew) {
	let		dom_item, dom_text, dom_remove;
	let		i;

	/// REMOVE MEMBERS
	while (dom_crew.childNodes.length > 0)
		dom_crew.removeChild(dom_crew.lastChild);
	/// ADD MEMBERS
	for (i = 0; i < crew.length; ++i) {
		/// CREATE ITEM
		dom_item = document.createElement('div');
		dom_item.className = 'member-item';
		/// CREATE ITEM CONTENT
		dom_text = document.createElement('div');
		dom_text.textContent = crew[i];
		dom_text.className = 'member-item-content';
		dom_item.appendChild(dom_text);
		dom_remove = document.createElement('div');
		dom_remove.textContent = 'SUPPRIMER';
		dom_remove.className = 'member-item-remove';
		dom_item.appendChild(dom_remove);
		/// ADD ITEM
		dom_crew.appendChild(dom_item);
		/// INIT REMOVE EVENT
		dom_item.addEventListener('click', function(e) {
			let		i;

			i = 0;
			while (e.path[i].className != 'member-item')
				i += 1;
			crew_del(dom_crew, crew, e.path[i]);
		});
	}
}

function	crew_del(dom_crew, crew, dom_member) {
	let		member_index;
	let		member;

	member = dom_member.childNodes[0].textContent;
	/// REMOVE FROM DOM
	dom_crew.removeChild(dom_member);
	/// REMOVE FROM JSON
	member_index = crew.indexOf(member);
	if (member_index >= 0)
		crew.splice(member_index, 1);
	/// REMOVE FROM SERVER
	data = JSON.stringify({"member": member});
	req = new XMLHttpRequest();
	req.open('DELETE', '/', true);
	req.setRequestHeader('Accept', 'application/json');
	req.setRequestHeader('Content-Type', 'application/json');
	req.onload = function(res) {
		console.log("POST RESPONSE");
		console.log(res);
	};
	req.send(data);
}

function	crew_add(dom_crew, crew, new_member) {
	let		req;
	let		data;

	/// CLEAN MEMBER NAME
	new_member = new_member[0].toUpperCase() + new_member.substring(1);
	/// ADD TO JSON
	crew.push(new_member);
	/// ADD TO DOM
	crew_update(dom_crew, crew);
	/// ADD TO SERVER
	data = JSON.stringify({"member": new_member});
	req = new XMLHttpRequest();
	req.open('POST', '/', true);
	req.setRequestHeader('Accept', 'application/json');
	req.setRequestHeader('Content-Type', 'application/json');
	req.onload = function(res) {
		console.log("POST RESPONSE");
		console.log(res);
	};
	req.send(data);
}

function	crew_check(crew, new_member) {
	let		i;

	new_member = new_member.trim();
	if (new_member == '')
		return false;
	for (i = 0; i < crew.length; ++i)
		if (crew[i] == new_member)
			return false;
	return true;
}

//document.addEventListener('load', function() {
document.addEventListener('DOMContentLoaded', function() {
	let		dom_crew;
	let		dom_form, dom_form_name;
	let		crew;
	let		req;

	/// INIT DOM
	dom_crew = document.getElementById('member-list');
	dom_form = document.getElementById('new-member-form');
	dom_form_name = document.getElementById('new-member-form-name');

	/// INIT NEW EVENT
	dom_form.addEventListener('submit', function(e) {
		let		new_member;
		let		data;

		e.preventDefault();
		new_member = dom_form_name.value.toLowerCase();
		if (crew_check(crew, new_member) == true)
			crew_add(dom_crew, crew, new_member);
		dom_form_name.value = '';
		dom_form_name.blur();
	});

	/// INIT CREW
	req = new XMLHttpRequest();
	req.open('GET', '/crew', true);
	req.onload = function() {
		crew = JSON.parse(req.responseText);
		crew_update(dom_crew, crew);
	};
	req.send(null);
});
