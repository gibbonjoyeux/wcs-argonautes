#!/usr/bin/env node

////////////////////////////////////////////////////////////////////////////////
/// MODULES
////////////////////////////////////////////////////////////////////////////////

						require('dotenv').config();
const	http =			require('http');
const	fs =			require('fs');
const	path =			require('path');
const	MongoClient =	require('mongodb').MongoClient;

////////////////////////////////////////////////////////////////////////////////
/// DATA
////////////////////////////////////////////////////////////////////////////////

const	PORT =		process.env.PORT | 8080;
const	USER =		process.env.USER;
const	PASSWORD =	process.env.PASSWORD;
const	URI = 'mongodb+srv://'
+ USER + ':' + PASSWORD
+ '@sandbox.t7ybc.mongodb.net/myFirstDatabase'
+ '?retryWrites=true&w=majority';
const	mongo = {};
let		crew = [];

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////
/// SERVER
//////////////////////////////////////////////////

function	server_crew(req, res) {
	res.statusCode = 200;
	res.setHeader('Content-Type', 'application/json');
	res.end(JSON.stringify(crew));
}

function	server_get(req, res) {
	let		url;
	let		ext;
	let		type;

	/// GET PATH
	url = req.url;
	if (url == '/')
		url = '/index.html';
	url = '.' + url;
	/// GET TYPE
	ext = path.extname(url);
	if (ext == '.html')
		type = 'text/html';
	else if (ext == '.css')
		type = 'text/css';
	else if (ext == '.js')
		type = 'application/javascript';
	else
		type = 'text/plain';
	/// GET FILE
	fs.readFile(url, 'utf8', function(err, data) {
		if (err) {
			res.statusCode = 404;
			res.end();
		} else {
			res.statusCode = 200;
			res.setHeader('Content-Type', type);
			res.end(data);
		}
	});
}

function	server_post(req, res) {
	let		data;

	data = '';
	req.on('data', function(chunk) {
		data += chunk;
	});
	req.on('end', async function(chunk) {
		data = JSON.parse(data);
		console.log(data);
		/// ADD MEMBER
		if (data && data.member) {
			await crew_add(data.member);
			res.statusCode = 200;
			res.setHeader('Content-Type', 'application/json');
			res.end(JSON.stringify({"success": true}));
		} else {
			res.statusCode = 404;
			res.setHeader('Content-Type', 'application/json');
			res.end(JSON.stringify({"success": false}));
		}
	});
}

function	server_delete(req, res) {
	let		data;

	data = '';
	req.on('data', function(chunk) {
		data += chunk;
	});
	req.on('end', async function(chunk) {
		data = JSON.parse(data);
		console.log(data);
		/// DEL MEMBER
		if (data && data.member) {
			await crew_del(data.member);
			res.statusCode = 200;
			res.setHeader('Content-Type', 'application/json');
			res.end(JSON.stringify({"success": true}));
		} else {
			res.statusCode = 404;
			res.setHeader('Content-Type', 'application/json');
			res.end(JSON.stringify({"success": false}));
		}
	});
}

function		server_main(req, res) {
	console.log(req.method);
	console.log(req.url);
	if (req.method == 'GET') {
		if (req.url == '/crew')
			server_crew(req, res);
		else
			server_get(req, res)
	} else if (req.method == 'POST') {
		server_post(req, res);
	} else if (req.method == 'DELETE') {
		server_delete(req, res);
	}
}

//////////////////////////////////////////////////
/// DATABASE
//////////////////////////////////////////////////

async function	crew_connect() {
	mongo.client = new MongoClient(URI);
	/// CONNECT
	await mongo.client.connect();
	/// RETRIEVE DATABASE
	mongo.db = mongo.client.db('odyssey');
	/// RETRIEVE COLLECTION
	mongo.col = mongo.db.collection('crew');
}

async function	crew_add(name) {
	let			doc;
	let			member;

	/// INIT DOC
	doc = {"name": name};
	/// CHECK DOC
	member = await mongo.col.findOne(doc);
	if (member)
		return false;
	/// ADD DOC
	await mongo.col.insertOne(doc);
	/// ADD MEMBER
	crew.push(name);
	return true;
}

async function	crew_del(name) {
	let			doc;
	let			member;
	let			member_index;

	/// INIT DOC
	doc = {"name": name};
	/// DELETE DOC
	await mongo.col.deleteOne(doc);
	/// REMOVE MEMBER
	member_index = crew.indexOf(name);
	if (member_index >= 0)
		crew.splice(member_index, 1);
}

async function	crew_list() {
	let			cursor;
	let			list, list_cleaned;
	let			i;

	/// GET DOCUMENTS
	cursor = mongo.col.find();
	list = await cursor.toArray();
	/// CLEAN LIST
	list_cleaned = [];
	for (i = 0; i < list.length; ++i)
		list_cleaned[i] = list[i].name;
	return list_cleaned;
}

////////////////////////////////////////////////////////////////////////////////
/// MAIN
////////////////////////////////////////////////////////////////////////////////

async function main() {
	let			server;

	try {
		/// CONECT DATABASE
		await crew_connect();
		console.log('SERVER CONNECTED TO DATABASE');
		/// RUN SERVER
		server = http.createServer(server_main);
		server.listen(PORT, async function() {
			console.log('SERVER RUNNING');
			crew = await crew_list();
			console.log(crew);
			//await crew_add("ulysse");
			//await crew_del("test");
		});
	} catch(err) {
		//await mongo.client.close();
		console.log("ERROR:");
		console.log(err);
	}
};
main();
